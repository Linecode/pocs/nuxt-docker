# Dockerfile
FROM node:18.17.1-alpine as base

RUN apk update && apk upgrade
RUN apk add git

FROM base as build

RUN mkdir -p /usr/src/nuxt-app
WORKDIR /usr/src/nuxt-app

COPY . /usr/src/nuxt-app/
RUN npm install
RUN npm run build

FROM base as runtime

COPY --from=build /usr/src/nuxt-app/.output /usr/src/nuxt-app/.output
WORKDIR /usr/src/nuxt-app

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

CMD [ "node", ".output/server/index.mjs" ]
